import { ChangeEvent, useEffect, useState } from "react";
import { shuffle } from "lodash";
import commonStyles from "../styles/Game.module.scss";
import { CardProps } from "./Card";
import dynamic from "next/dynamic";
import { IconButton, makeStyles, Mark, Slider, Theme } from "@material-ui/core";
import { Autorenew, Style } from "@material-ui/icons";
import DrawerMenu from "./DrawerMenu";

export interface GameProps {
  cardsValues: Array<CardsValues>;
}

export interface CardsValues {
  theme: string;
  representativeValue: string;
  values: Array<string>;
}

const useStyles = makeStyles((theme: Theme) => ({
  slider: {
    color: theme.palette.primary.main,
    width: "30%",
    position: "relative",
    top: "5px",
  },
  iconBtn: {
    color: theme.palette.primary.main,
    position: "relative",
    top: "-5px",
  },
}));

const createCards = (
  cardsValues: Array<string>,
  gridSideSize: number
): Array<CardProps> => {
  const cards: Array<CardProps> = [];
  const numberOfPairsToFind = Math.round((gridSideSize * gridSideSize) / 2);
  const cardsValuesSplited = shuffle(cardsValues).slice(0, numberOfPairsToFind);
  const cardsValuesDoubledAndShuffled = shuffle([
    ...cardsValuesSplited,
    ...cardsValuesSplited,
  ]);
  cardsValuesDoubledAndShuffled.map((cardsValues: string, index: number) => {
    cards.push({
      id: index,
      value: cardsValues,
      flipped: false,
      found: false,
      wrong: false,
      sideSize: 0,
      beeingShuffled: false,
      onCardClicked: null,
    });
  });
  return cards;
};

// to avoid a window access on the SSR
const GameGrid = dynamic(() => import("./GameGrid"), {
  ssr: false,
});

const getCardsValuesFromTheme = (
  cardsValues: Array<CardsValues>,
  theme: string
): Array<string> => {
  return (
    cardsValues.find((cardsValue) => cardsValue.theme === theme).values || []
  );
};

const Game = ({ cardsValues }: GameProps) => {
  const classes = useStyles();
  const [drawerMenuOpen, setDrawerMenuOpen] = useState<boolean>(false);
  const [gridSideSize, setGridSideSize] = useState(4);
  const [cardsValuesTheme, setCardsValuesTheme] = useState<string>("food");
  const [cards, setCards] = useState<Array<CardProps>>(
    createCards(
      getCardsValuesFromTheme(cardsValues, cardsValuesTheme),
      gridSideSize
    )
  );
  const [numberOfWrongPairs, setNumberOfWrongPairs] = useState(0);
  useEffect(() => {
    resetGrid(gridSideSize);
  }, [cardsValuesTheme]);

  const getSideSizeAvailables = (): Array<Mark> => {
    const sideSizesMarks = [];
    const sideSizes = [2, 4, 6, 8];
    sideSizes.map((ss) => {
      sideSizesMarks.push({ value: ss, label: `${ss}x${ss}` });
    });
    return sideSizesMarks;
  };

  const getSideSizeAvailablesMin = (): number => {
    return getSideSizeAvailables().reduce((previous, current) =>
      previous.value < current.value ? previous : current
    ).value;
  };

  const getSideSizeAvailablesMax = (): number => {
    return getSideSizeAvailables().reduce((previous, current) =>
      previous.value > current.value ? previous : current
    ).value;
  };

  const handleGridSizeSliderChange = (
    event: ChangeEvent,
    newGridSideSize: number
  ): void => {
    if (newGridSideSize === gridSideSize) {
      // if the user tried to change the grid side size but finally let it to the current value,
      // we don't wont to reset the grid
      return;
    }
    setGridSideSize(newGridSideSize);
    resetGrid(newGridSideSize, false);
  };

  const resetGrid = (gridSideSize: number, animation: boolean = true): void => {
    const newCardsUnFlipped = [...cards];
    // unflipp every cards, then wait the time of the transition is over to recreate the grid with the new values
    // so the user won't see the new value before the card is un flipped !
    // 500ms is the time of the transition in the scss file
    newCardsUnFlipped.map((card) => {
      card.flipped = false;
      if (animation) {
        card.beeingShuffled = true;
      }
    });
    setCards(newCardsUnFlipped);
    const delay = animation ? 500 : 0;
    setTimeout(() => {
      setCards(
        createCards(
          getCardsValuesFromTheme(cardsValues, cardsValuesTheme),
          gridSideSize
        )
      );
    }, delay);
    setNumberOfWrongPairs(0);
  };

  return (
    <div className={commonStyles.container}>
      <div>
        <h1 className={commonStyles.title}>Memory Game 🧠</h1>
        <GameGrid
          cards={cards}
          gridSideSize={gridSideSize}
          onCardsChanged={(cards) => setCards(cards)}
          onWrongPair={() => setNumberOfWrongPairs(numberOfWrongPairs + 1)}
        />
        <div className={commonStyles.wrongPairsContainer}>
          <p>Wrong pairs:{numberOfWrongPairs}</p>
          <Slider
            className={classes.slider}
            defaultValue={4}
            aria-labelledby="discrete-slider-small-steps"
            step={null}
            marks={getSideSizeAvailables()}
            min={getSideSizeAvailablesMin()}
            max={getSideSizeAvailablesMax()}
            valueLabelDisplay="auto"
            onChangeCommitted={handleGridSizeSliderChange}
          />
          <IconButton
            className={classes.iconBtn}
            aria-label="Emoji theme"
            onClick={() => setDrawerMenuOpen(!drawerMenuOpen)}
          >
            <Style />
          </IconButton>
          <IconButton
            className={classes.iconBtn}
            aria-label="new game"
            onClick={() => resetGrid(gridSideSize)}
          >
            <Autorenew />
          </IconButton>
          <DrawerMenu
            displayed={drawerMenuOpen}
            cardsValues={cardsValues}
            currentCardsValuesTheme={cardsValuesTheme}
            onThemeChanged={(newTheme) => setCardsValuesTheme(newTheme)}
            onClose={() => setDrawerMenuOpen(false)}
          />
        </div>
      </div>
    </div>
  );
};

export default Game;
