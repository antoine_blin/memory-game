import Card, { CardProps } from "./Card";
import commonStyles from "../styles/GameGrid.module.scss";
import { CSSProperties, useEffect, useState } from "react";
import { useTheme } from "@material-ui/core";

const WRONG_PAUSE_DELAY = 500;
const HEIGHT_OF_ELEMENTS_OUTSIDE_THE_GRID = 140;

export interface GameGridProps {
  cards: Array<CardProps>;
  gridSideSize: number;
  onCardsChanged: (cards: Array<CardProps>) => void;
  onWrongPair: () => void;
}

const GameGrid = ({
  cards,
  gridSideSize,
  onCardsChanged,
  onWrongPair,
}: GameGridProps) => {
  const theme = useTheme();
  const [screenDimensions, setScreenDimensions] = useState<{
    height: number;
    width: number;
  }>({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    const resizeListener = () => {
      setScreenDimensions({
        height: window.innerHeight,
        width: window.innerWidth,
      });
    };
    window.addEventListener("resize", resizeListener);
    return () => {
      window.removeEventListener("resize", resizeListener);
    };
  }, []);

  const onCardClicked = (id: number, value: string) => {
    const clickedCard = cards[id];
    const newCards = [...cards];
    // if a card is already flipped :
    const previousCard = cards.find((card) => card.flipped && !card.found);
    if (
      (previousCard && clickedCard.id === previousCard.id) ||
      clickedCard.flipped ||
      clickedCard.found
    ) {
      return;
    }
    if (previousCard) {
      if (clickedCard.value === previousCard.value) {
        // set cards as found
        newCards[clickedCard.id] = {
          ...newCards[clickedCard.id],
          flipped: true,
          found: true,
        };
        newCards[previousCard.id] = {
          ...newCards[previousCard.id],
          found: true,
        };
      } else {
        newCards[clickedCard.id] = {
          ...newCards[clickedCard.id],
          flipped: true,
          wrong: true,
        };
        newCards[previousCard.id] = {
          ...newCards[previousCard.id],
          wrong: true,
        };
        // set a callback to set back cards as un flipped after the pause delay
        setTimeout(() => {
          handleWrongPairCallback(previousCard, clickedCard);
        }, WRONG_PAUSE_DELAY);
      }
    } else {
      // set this new card as flipped
      newCards[clickedCard.id] = { ...newCards[clickedCard.id], flipped: true };
    }
    onCardsChanged(newCards);
  };

  const handleWrongPairCallback = (
    firstCard: CardProps,
    secondCard: CardProps
  ) => {
    const newCards = [...cards];
    newCards[firstCard.id] = {
      ...firstCard,
      flipped: false,
      wrong: false,
    };
    newCards[secondCard.id] = {
      ...newCards[secondCard.id],
      flipped: false,
      wrong: false,
    };
    onWrongPair();
    onCardsChanged(newCards);
  };

  const isGridCompleted = (): boolean => {
    return cards.find((card) => !card.found) ? false : true;
  };

  // allow to get the number of grid columns depending on the number of cards
  // the col width is passed to the card to be able to have a css animation
  // since grid-template-columns animation is not available...
  const getGridStyle = (): CSSProperties => {
    const colsNumber = Math.round(Math.sqrt(cards.length));
    const gridStyle: CSSProperties = {};
    gridStyle.gridTemplateColumns = `repeat(${colsNumber}, auto`;
    gridStyle.gridTemplateRows = `repeat(${colsNumber}, auto`;
    gridStyle.fontSize = `${getCardSideSize() / 30}rem`;
    if (isGridCompleted()) {
      gridStyle.backgroundColor = theme.palette.primary.main;
    }
    return gridStyle;
  };

  const getCardSideSize = (): number => {
    let screenMinSideSize = Math.min(
      screenDimensions.height,
      screenDimensions.width
    );
    if (
      screenDimensions.height <=
      screenDimensions.width + HEIGHT_OF_ELEMENTS_OUTSIDE_THE_GRID
    ) {
      // in landscape mode title and buttons bar have to be displayed
      screenMinSideSize =
        screenMinSideSize - HEIGHT_OF_ELEMENTS_OUTSIDE_THE_GRID;
    }
    const cardSize = screenMinSideSize / gridSideSize;
    return cardSize;
  };

  return (
    <div className={commonStyles.gridContainer} style={getGridStyle()}>
      {cards.map((card) => (
        <Card
          key={card.id}
          id={card.id}
          value={card.value}
          flipped={card.flipped}
          found={card.found}
          wrong={card.wrong}
          sideSize={getCardSideSize()}
          beeingShuffled={card.beeingShuffled}
          onCardClicked={(id, value) => onCardClicked(id, value)}
        />
      ))}
    </div>
  );
};

export default GameGrid;
